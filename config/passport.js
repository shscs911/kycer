const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Farmer = require('../routes/farmer');
module.exports = function(passport){
    passport.use(
        new LocalStrategy({usernameField: 'username'},(username,password,done)=>{
            Farmer.findOne({username:username})
            .then(user=>{console.log(user)
                if(!user){
                return done(null,false,{message:'Username not registered'});
             }
        bcrypt.compare(password,user.password,(err,isMatch)=>{
            if(err) throw err;
            if(isMatch){
                console.log("success");
                return done(null,user);
            }else{
                return done(null,false,{message:'password incorrect'})
            }

        })
        
        }).catch(err=>console.log(err));
        })
    )
    passport.serializeUser(function(user, done) {
        done(null, user.id);
      });
      
      passport.deserializeUser(function(id, done) {
        Farmer.findById(id, function(err, user) {
          done(err, user);
        });
      });

}