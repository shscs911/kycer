var express = require("express");
var app = express();
var bodyparser = require("body-parser");
app.use(bodyparser.urlencoded({ extended: false }));

app.set("view engine", "ejs");

var index = require("./routes/index");

var port = process.env.PORT || 3000;

app.listen(port, function() {
  console.log("App listening on port " + port);
});

app.use("/", index);
