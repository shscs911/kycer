var express = require("express");
const mongoose = require("mongoose");
const session = require("express-session");
const multer = require("multer");
const passport = require("passport");
const bcrypt = require("bcryptjs");
require("../config/passport")(passport);

var router = express.Router();
router.use(express.json());
router.use(express.urlencoded({ extended: true }));
router.use(
  session({ secret: "eyesonly", saveUninitialized: false, resave: false })
);
router.use(passport.initialize());
router.use(passport.session());
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads");
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now());
  }
});

var upload = multer({ storage: storage });

var url =
  "mongodb+srv://shscs112:shscs112@kyc-fklxm.mongodb.net/test?retryWrites=true&w=majority";
mongoose
  .connect(url, { useNewUrlParser: true, autoIndex: false })
  .then(console.log("Database connection successful"))
  .catch(e => {
    console.log(e);
  });

var Farmer = require("./farmer");

router.get("/", function(req, res) {
  req.session.usr = null;
  res.render("home");
});

router.get("/profile", function(req, res) {
  console.log("session profile:", req.session.usr);
  res.render("profile", { result: req.user });
});

router.get("/register", function(req, res) {
  res.render("register");
});

router.post("/register", upload.single("photo"), function(req, res) {
  var name = req.body.name;
  var email = req.body.email;
  var aadhar = req.body.aadhar;
  var kissan = req.body.kissan;
  var gender = req.body.gender;
  var state = req.body.state;
  var district = req.body.district;
  var taluk = req.body.taluk;
  var village = req.body.village;
  var uname = req.body.username;
  var pwd = req.body.pwd;
  var repwd = req.body.repwd;
  var dob = req.body.dob;

  if (pwd != repwd) {
    console.log("password mismatch");
    res.redirect("/register");
  } else {
    var newFarmer = Farmer({
      username: uname,
      password: pwd,
      name: name,
      gender: gender,
      state: state,
      district: district,
      taluk: taluk,
      village: village,
      dob: dob,
      email: email,
      aadhar: aadhar,
      kissan: kissan
    });
    bcrypt.genSalt(10, (err, salt) =>
      bcrypt.hash(newFarmer.password, salt, (err, hash) => {
        if (err) throw err;
        console.log(hash);
        newFarmer.password = hash;
        console.log("Profile" + newFarmer);
        newFarmer.save(function(err) {
          if (err) throw err;
          console.log("Created new entry");

          res.redirect("/");
        });
      })
    );
  }
});

router.post("/login", (req, res, next) => {
  console.log(req.body.username);
  passport.authenticate("local", {
    successRedirect: "/profile",
    failureRedirect: "/"
  })(req, res, next);
});

module.exports = router;
