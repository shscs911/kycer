const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// create a schema
const farmerSchema = new Schema(
  {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    name: String,
    gender: String,
    state: String,
    district: String,
    taluk: String,
    village: String,
    dob: String,
    email: String,
    aadhar: String,
    kissan: String,
    photo: String
  },
  { collection: "farmers" }
);

const Farmer = mongoose.model("Farmer", farmerSchema);

module.exports = Farmer;
